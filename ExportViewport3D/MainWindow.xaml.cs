﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace ExportViewport3D
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private double dpiX = 96.0;
        private double dpiY = 96.0;

        private void ExportViewportClick(object sender, RoutedEventArgs e)
        {
            VisualDirectly();
            //VisualWithVisualBrush();
        }

        private void VisualDirectly()
        {
            Rect bounds = VisualTreeHelper.GetDescendantBounds(viewport);
            Rect rect = new Rect(new Point(), bounds.Size);
            RenderTargetBitmap tbmp = new RenderTargetBitmap((int)(rect.Width * dpiX / 96.0), (int)(rect.Height * dpiY / 96.0), dpiX, dpiY, PixelFormats.Default);
            tbmp.Render(viewport);
        }

        private void VisualWithVisualBrush()
        {
            var target = viewport;
            Rect bounds = VisualTreeHelper.GetDescendantBounds(target);
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)(bounds.Width * dpiX / 96.0), (int)(bounds.Height * dpiY / 96.0), dpiX, dpiY, PixelFormats.Pbgra32);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext ctx = dv.RenderOpen())
            {
                VisualBrush vb = new VisualBrush(target);
                ctx.DrawRectangle(vb, null, new Rect(new Point(), bounds.Size));
            }
            rtb.Render(dv);
        }

        private void ShowHideClick(object sender, RoutedEventArgs e)
        {
            button3D.Visibility = (button3D.Visibility == System.Windows.Visibility.Visible) ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
        }

        private void button3D_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Click registered");
        }
    }
}
